# Todo Backend-Agustin Arce

Backend RESTful para la aplicación de practica del Módulo de Gestión de Proyectos de Software

## Dependencias

- Python 3.6+

## Set up
Preparar un entorno virtual nuevo a fin de resolver las dependencias en el mismo
 - ### Opcion 1
    Crear entorno
    ```bash
    python -m venv [nombre entorno]
    ```
    Activar
    ```bash
    . [nombre entorno]/bin/activate
    ```
- ### Opcion 2
    ```bash
    $ virtualenv venv
    ```
    Activar
    ```bash
    . [nombre entorno]/bin/activate
    ```

Resolver las dependencias de python usando el arhivo `requirements.txt`

```bash
$ pip install -r requirements.txt
```

## Uso

Para iniciar el servidor edite las opciones en `app.py` y luego ejecútelo

```bash
$ python app.py
```
o
```bash
$ flask run
```
