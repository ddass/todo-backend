from flask_restful.reqparse import Namespace
from db import db
from models.type import TypeModel
from utils import _assign_if_something

class TaskModel(db.Model):
    __tablename__ = 'tasks'

    id = db.Column(db.BigInteger, primary_key = True)
    description = db.Column(db.String)
    status = db.Column(db.String)
    id_type = db.Column(db.BigInteger,db.ForeignKey(TypeModel.id))
    _type = db.relationship('TypeModel', uselist=False, primaryjoin='TypeModel.id == TaskModel.id_type', foreign_keys='TaskModel.id_type')

    def __init__(self, id, description, status, id_type):
        self.id = id
        self.description = description
        self.status = status
        self.id_type = id_type
    
    def json(self, jsondepth = 0):

        json = {
            'id': self.id,
            'description': self.description,
            'status': self.status,
            "id_type":self.id_type
        }
        
        if jsondepth > 0:
            if self._type:
                json['_type'] = self._type.json(jsondepth)

        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id = id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['description','status','id_type']:
            _assign_if_something(self, newdata, no_pk_key)