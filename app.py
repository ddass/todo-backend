import logging
import os
from resources.task import Task, TaskList, TaskSearch
from resources.type import Type, TypeList, TypeSearch
from flask import Flask, redirect, jsonify
from flask_restful import Api
from flasgger import Swagger
from db import db
from flask_cros import CORS

app = Flask(__name__)

api = Api(app, errors={
    'NoAuthorizationError': {
        "message": "La respueta no tiene token",
        "error": "authorization_required",
        "status": 401
    }
})

PREFIX = os.environ.get('PREFIX_PATH','/api')

# Swagger config
app.config['SWAGGER'] = {
    'title': 'todo-backend',
    'version': '1.0.0',
    'description': 'API de servicios REST en Flask',
    'uiversion': 2,
    'tags': [{'name': 'jwt'}],
    'specs': [{
        'endpoint': 'apispec_1',
        'route': f'{PREFIX}/apispec_1.json',
        'rule_filter': lambda rule: True,  # all in
        'model_filter': lambda tag: True  # all in
    }],
    'specs_route': f"{PREFIX}/apidocs/",
    'static_url_path': f'{PREFIX}/static'
}

swagger = Swagger(app)

app.logger.setLevel(logging.INFO)

def env_config(name, default):
    app.config[name] = os.environ.get(name, default = default)

# Database config
env_config('SQLALCHEMY_DATABASE_URI', 'postgresql://postgres:postgres@localhost:5432/todo')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['SQLALCHEMY_ECHO'] = False

@app.route("/")
@app.route(f'{PREFIX}')
def welcome():
    return redirect(f"{PREFIX}/apidocs", code= 302)

# Enable CORS
CORS(app,resources={r'/*':{'origins': '*'}})
@app.route(f'{PREFIX}/hello', methods=['GET'])
def hello():
    return jsonify('Word!')


# URIs
api.add_resource(Task, f'{PREFIX}/tasks/<id>')
api.add_resource(TaskList, f'{PREFIX}/tasks')
api.add_resource(TaskSearch, f'{PREFIX}/search/tasks')
api.add_resource(Type, f'{PREFIX}/types/<id>')
api.add_resource(TypeList, f'{PREFIX}/types')
api.add_resource(TypeSearch, f'{PREFIX}/search/types')

if __name__ == '__main__':
    db.init_app(app)
    app.run(debug=True)
else: 
    db.init_app(app)
